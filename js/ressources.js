var uri_materials = "https://api.guildwars2.com/v2/materials";
var uri_items     = "https://api.guildwars2.com/v2/items";

function setCategoryName(catid) {
	$.getJSON({
		url: uri_materials+'/'+catid+'.json',
		type: "get",
		dataType: "json"
	}).done(function(data) {
		$("<summary>"+data.name+"</summary>").appendTo("#cat_"+catid);
		$("<ul id='list_cat_"+catid+"'>").appendTo("#cat_"+catid);
	});
}

function getItemList(catid) {
	$.getJSON({
		url: uri_materials+'/'+catid+'.json',
		type: "get",
		dataType: "json"
	}).done(function(data) {
		$.each(data.items, function(i, itemid) {
			setItems(itemid, catid);
		});
	});
}

function setItems(itemid, catid) {
	$.getJSON({
		url: uri_items+'/'+itemid+'.json',
		type: "get",
		dataType: "json"
	}).done(function(data) {
		$('<li class="pbs prs inbl"><img src="'+data.icon+'" width="28px" height="28px" alt="'+data.name.toString()+'" title="'+data.name.toString()+'"></li>').appendTo("#list_cat_"+catid);
	});
}

$.ajax({
	url: uri_materials+'.json',
	type: "get",
	dataType: "json"
})
.done(function(data) {
	$.each(data, function(i, catid) {
		$("<details id='cat_"+catid+"'>").appendTo( "#materials" );
		setCategoryName(catid);
		getItemList(catid);
		$( "</ul>" ).appendTo( "#cat_"+catid );
		$('</details>').appendTo( "#materials" );
 	});
});
