var uri_dailys       = "https://api.guildwars2.com/v2/achievements/daily";
var uri_achievements = "https://api.guildwars2.com/v2/achievements";

function setSuccess(success_id, num) {
	$.getJSON({
		url: uri_achievements+'/'+success_id+'.json',
		type: "get",
		dataType: "json"
	}).done(function(data) {
		$("<li>"+data.name.toString()+"</li>").appendTo("#success_list_"+num);
	});
}

$.ajax({
	url: uri_dailys+'.json',
	type: "get",
	dataType: "json"
})
.done(function(data) {
	var c = 0;
	$.each(data, function(name, object) {
		// console.log(object);
		$("<details id='success_"+c+"' class='plm'>").appendTo("#achievements");
		$("<summary>"+name+"</summary>").appendTo("#success_"+c);
		$("<ul id='success_list_"+c+"' class='mlm mrm unstyled'>").appendTo("#success_"+c);
			$.each(object, function(i, item) {
				setSuccess(item.id, c);
			});
		$("</ul>").appendTo("#success_"+c);
		$('</details>').appendTo( "#achievements" );
		c++;
 	});
});
